# Deployr
![Private](https://img.shields.io/badge/-private-important)
[![Version](https://img.shields.io/badge/version-v2.0.0-blue)](https://gitlab.com/kadesign/deployr-bot/-/tags/2.0.0)
[![Pipeline status](https://gitlab.com/kadesign/deployr-bot/badges/2.0.0/pipeline.svg)](https://gitlab.com/kadesign/deployr-bot/-/commits/2.0.0)
![Maintenance status](https://img.shields.io/maintenance/yes/2024)

Бот для Телеграма, оповещающий о статусе развертывания приложений в Netlify.

## Требования
* node (22.11.0)

## Как запустить
Перед запуском необходимо прописать переменные окружения (см. ниже, можно в файл `.env`).

Бот должен быть добавлен в канал, куда будут присылаться уведомления, и назначен администратором с минимальным правом рассылать сообщения.

## Переменные окружения
Переменная|Обязательная?|Описание
----------|--------------|--------
**BOT_TOKEN**|*да*|Токен бота (необходимо получить у [BotFather](https://t.me/BotFather))
**BOT_NAME**|*да*|Название бота (никнейм, сконфигурированный у того же BotFather)
**BOT_PORT**|нет|Порт, который бот будет слушать (по умолчанию **3000**)
**PROXY_HOST**|нет|Хост socks5-прокси
**PROXY_PORT**|нет|Порт socks5-прокси *(обязателен, если указан хост)*
**PROXY_USER**|нет|Логин для socks5-прокси *(обязателен, если указаны хост и порт)*
**PROXY_PASSWORD**|нет|Пароль к socks5-прокси *(обязателен, если указаны хост и порт)*
**SENTRY_DSN**|нет|DSN для сервиса Sentry (если нужен для отлавливания ошибок, которые не были обработаны)
**CHAT_ID**|**да**|Telegram ID чата, в который будут отправляться уведомления
