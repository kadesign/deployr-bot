export default class Formatter {
  static formatCommitRef(request) {
    const commitRef = request.commit_ref ? request.commit_ref.substring(0,7) : 'HEAD';
    const commitMessage = request.title ? `(${request.title})` : '';

    return `${request.branch}@[${commitRef}](${request.commit_url}) ${commitMessage}`
  }

  static formatDeployTime(deployTimeInSec) {
    const minutes = Math.floor(deployTimeInSec / 60);
    return `${minutes > 0 ? minutes + 'm ' : ''}${deployTimeInSec % 60}s`;
  }
}
