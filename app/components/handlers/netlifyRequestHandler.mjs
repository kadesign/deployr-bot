import MicroLog from '@kadesign/microlog';

import DeployFinishedMessage from '../../models/messages/deployFinishedMessage.mjs';
import DeployLockedMessage from '../../models/messages/deployLockedMessage.mjs';
import DeployStartedMessage from '../../models/messages/deployStartedMessage.mjs';
import DeployUnlockedMessage from '../../models/messages/deployUnlockedMessage.mjs';
import ErrorMessage from '../../models/messages/errorMessage.mjs';
import Bot from '../bot/bot.mjs';

export default class NetlifyRequestHandler {
  #logger;
  #bot;

  constructor() {
    this.#logger = new MicroLog(this).setOutputDateInUTC();
    this.#bot = new Bot();
  }

  async handle(request) {
    if (!request) {
      this.#logger.log('Got empty request, it was ignored');
      return;
    }

    let message;

    this.#logger.info(`Incoming request: app = "${request.name}", state = "${request.state}", branch = "${request.branch}", commitRef = ${request.commit_ref || 'HEAD'}, locked = ${request.locked}`);

    switch (request.state) {
      case 'building':
        message = new DeployStartedMessage(request);
        break;
      case 'error':
        message = new ErrorMessage(request);
        break;
      case 'ready':
        if (request.locked === null) {
          message = new DeployFinishedMessage(request);
        } else if (request.locked) {
          message = new DeployLockedMessage(request);
        } else {
          message = new DeployUnlockedMessage(request);
        }
        break;
      default:
        this.#logger.warn('Request ignored');
        return;
    }

    await this.#bot.say(message, true);
  }
}
