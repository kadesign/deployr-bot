import MicroLog from '@kadesign/microlog';
import SocksAgent from 'socks5-https-client/lib/Agent.js';
import { checkEnvVariables, escapeUnderscore } from '@kadesign/toolbox-js';
import { Telegraf } from 'telegraf';

let instance;

export default class Bot {
  #logger;
  #telegraf;

  constructor() {
    if (instance) return instance;
    instance = this;

    this.#logger = new MicroLog(this).setOutputDateInUTC();
    this.#initTelegraf();
  }

  #initTelegraf() {
    checkEnvVariables('BOT_NAME', 'BOT_TOKEN', 'CHAT_ID');

    let botOpts;
    if (process.env.PROXY_HOST && process.env.PROXY_PORT) {
      const proxyAgentOpts = {
        socksHost: process.env.PROXY_HOST,
        socksPort: process.env.PROXY_PORT
      };

      if (process.env.PROXY_USER) proxyAgentOpts.socksUser = process.env.PROXY_USER;
      if (process.env.PROXY_PASSWORD) proxyAgentOpts.socksPassword = process.env.PROXY_PASSWORD;

      botOpts = {
        username: process.env.BOT_NAME,
        telegram: {
          agent: new SocksAgent(proxyAgentOpts)
        }
      };
    } else {
      botOpts = {
        username: process.env.BOT_NAME
      };
    }

    this.#telegraf = new Telegraf(process.env.BOT_TOKEN, botOpts);
  }

  async say(message, markdown = false) {
    await this.#telegraf.telegram.sendMessage(process.env.CHAT_ID, escapeUnderscore(message.formattedText), {
      parse_mode: (markdown ? 'Markdown' : undefined),
      disable_web_page_preview: true,
      reply_markup: message.buttons ? {inline_keyboard: [message.buttons]} : undefined
    });
    this.#logger.info(`Message ${message.code} was sent to the chat`);
  }

  async checkApi() {
    const startTime = new Date();
    let botInfo;

    try {
      botInfo = await this.#telegraf.telegram.getMe();
    } catch (err) {
      if (err.code === 'ENOTFOUND' || err.code === 'ECONNRESET') throw new Error('Telegram API error: Not available');
      if (err.code === 401) throw new Error('Telegram API error: Invalid token');
      throw new Error(`Telegram API error: ${err.message}`);
    }

    if (botInfo) {
      const endTime = new Date();
      this.#logger.info(`Telegram API connection OK, ping: ${(endTime - startTime).toString()}ms`);
      if (process.env.BOT_NAME !== botInfo.username) throw new Error('Telegram API error: Wrong bot name configured');
      this.#logger.info('Telegram API token check OK');
    }
  }
}
