import * as Sentry from '@sentry/node';
import MicroLog from '@kadesign/microlog';

import Bot from './components/bot/bot.mjs';
import Server from './server/server.mjs';

if (process.env.SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.SENTRY_DSN,
    environment: process.env.NODE_ENV ? process.env.NODE_ENV : undefined
  });
}

// Main workflow
const logger = new MicroLog().setOutputDateInUTC();
logger.info(`${process.env.BOT_NAME} is started`);
logger.info(`Current version: ${process.env.npm_package_version}`);
if (process.env.NODE_ENV) logger.info(`Application is running in ${process.env.NODE_ENV} mode`);

const bot = new Bot();
await bot.checkApi();

const server = new Server();
await server.init();
server.listen();
