import BaseMessage from './baseMessage.mjs';

export default class DeployUnlockedMessage extends BaseMessage {
  constructor(request) {
    const header = `${request.name} is unlocked`;
    const bodyText = `*Notice:* latest deploy wasn't published automatically.\nGo to control panel to publish it manually or push other changes to trigger publishing.`;
    const buttons = [
      {
        icon: '🌍',
        text: 'Open app',
        url: request.url
      },
      {
        icon: '🔧',
        text: 'Deploys',
        url: `${request.admin_url}/deploys`
      }
    ];
    super('unlocked', { icon: '🔓', text: header }, bodyText, buttons);
  }
}
