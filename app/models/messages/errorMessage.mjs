import BaseMessage from './baseMessage.mjs';
import formatter from '../../utils/formatter.mjs';

export default class ErrorMessage extends BaseMessage {
  constructor(request) {
    const header = `Deploy of ${request.name} failed`;
    const bodyText = `*Built from:* ${formatter.formatCommitRef(request)}\n*Error:* ${request.error_message}`;
    const buttons = [
      {
        icon: '🌍',
        text: 'Latest successful build',
        url: request.url
      },
      {
        icon: '📝',
        text: 'Log',
        url: `${request.admin_url}/deploys/${request.id}`
      }
    ];
    super('error', { icon: '🔥', text: header }, bodyText, buttons);
  }
}
