import BaseMessage from './baseMessage.mjs';
import formatter from '../../utils/formatter.mjs';

export default class DeployLockedMessage extends BaseMessage {
  constructor(request) {
    const header = `${request.name} is locked`;
    const bodyText = `Further deploys won't be published automatically.\n\n*Latest published commit:* ${formatter.formatCommitRef(request)}`;
    const buttons = [
      {
        icon: '🌍',
        text: 'Open app',
        url: request.url
      },
      {
        icon: '🔧',
        text: 'Deploys',
        url: `${request.admin_url}/deploys`
      }
    ];
    super('locked', { icon: '🔒', text: header }, bodyText, buttons);
  }
}
