import BaseMessage from './baseMessage.mjs';
import formatter from '../../utils/formatter.mjs';

export default class DeployFinishedMessage extends BaseMessage {
  constructor(request) {
    const header = `${request.name} is live`;
    const bodyText = `*Built from:* ${formatter.formatCommitRef(request)}\n*Deployed in:* ${formatter.formatDeployTime(request.deploy_time)}`;
    const buttons = [
      {
        icon: '🌍',
        text: 'Open app',
        url: request.url
      },
      {
        icon: '📝',
        text: 'Log',
        url: `${request.admin_url}/deploys/${request.id}`
      }
    ];
    super('finished', { icon: '🚀', text: header }, bodyText, buttons);
  }
}
