export default class BaseMessage {
  code;
  #header;
  #text;
  #buttons;

  constructor(code, header, text, buttons) {
    this.code = code;
    this.#header = header;
    this.#text = text;
    this.#buttons = buttons;
  }

  get formattedText() {
    return `${this.#header.icon} *${this.#header.text}*\n\n${this.#text}`
  }

  get buttons() {
    const buttons = [];
    this.#buttons.forEach(btn => {
      buttons.push({
        text: `${btn.icon || ''} ${btn.text}`.trim(),
        url: btn.url
      });
    });
    return buttons;
  }
}
