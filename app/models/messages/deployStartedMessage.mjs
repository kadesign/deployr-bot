import BaseMessage from './baseMessage.mjs';

export default class DeployStartedMessage extends BaseMessage {
  constructor(request) {
    const header = `Deploy of ${request.name} started`;
    const bodyText = `*Branch:* ${request.branch}`;
    const buttons = [
      {
        icon: '📝',
        text: 'Log',
        url: `${request.admin_url}/deploys/${request.id}`
      }
    ];
    super('started', { icon: '⏳', text: header }, bodyText, buttons);
  }
}
