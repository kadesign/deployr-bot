import path from 'path';
import express from 'express';
import MicroLog from '@kadesign/microlog';
import { globSync } from 'glob';
import BaseRouter from './routes/baseRouter.mjs';

let instance;

export default class Server {
  #server;
  /**
   * @type MicroLog
   */
  #logger;

  constructor() {
    if (instance) return instance;

    this.#logger = new MicroLog(this).setOutputDateInUTC();
    this.#server = express();

    instance = this;
  }

  async init() {
    this.#server.use(express.json());

    // routes
    await this.#initRoutes();

    // error handling
    this.#server.use((err, req, res, _next) => {
      this.#logger.error(err);
      res.end();
    });
  }

  listen() {
    const port = process.env.API_PORT || 3000;
    this.#server.listen(port, () => {
      this.#logger.info(`Listening on port ${port}`);
    });
  }

  async #initRoutes() {
    this.#logger.info('Initializing routes started');
    for (const c of globSync('./routes/impl/*', {
      cwd: path.resolve(process.cwd(), 'app/server'),
      dotRelative: true
    })) {
      const { default: ComponentRouter } = await import(c);
      if (ComponentRouter.prototype instanceof BaseRouter) {
        const router = new ComponentRouter();
        this.#server.use(router.baseRoute, router.router);
        this.#logger.debug(`Route ${router.baseRoute} initialized`);
      }
    }
    this.#logger.info('Initializing routes finished');
  }
}
