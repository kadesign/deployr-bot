import express from 'express';

import NetlifyRequestHandler from '../../../components/handlers/netlifyRequestHandler.mjs';
import BaseRouter from '../baseRouter.mjs';

export default class NetlifyRouter extends BaseRouter {
  #requestHandler;

  constructor() {
    super(express.Router(), '/netlify');
    this.#requestHandler = new NetlifyRequestHandler();

    this.#init();
  }

  #init() {
    this.router.post('/', async (req, res) => {
      await this.#requestHandler.handle(req.body);
      res.end();
    });
  }
}
