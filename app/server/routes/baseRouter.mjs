import MicroLog from '@kadesign/microlog';

export default class BaseRouter {
  #router;
  #logger;
  #baseRoute;

  constructor(router, baseRoute) {
    this.#router = router;
    this.#baseRoute = baseRoute;
    this.#logger = new MicroLog(this);
  }

  get router() {
    return this.#router;
  }

  get logger() {
    return this.#logger;
  }

  get baseRoute() {
    return this.#baseRoute;
  }
}
