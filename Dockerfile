FROM node:22.11.0-alpine AS builder

WORKDIR /opt/kadesign/deployr-bot

COPY . .
RUN yarn --prod --ignore-optional

###

FROM node:22.11.0-alpine AS production-image

ENV PORT=3000

WORKDIR /opt/kadesign/deployr-bot

COPY --from=builder /opt/kadesign/deployr-bot ./

ENTRYPOINT [ "yarn", "start" ]
EXPOSE $PORT
